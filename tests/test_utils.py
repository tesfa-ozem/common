import random
import string
import uuid

from common.utils import filter_uuids


def test_uuids_filter():
    uuids = [uuid.uuid4() for _ in range(10)]
    assert uuids == list(filter_uuids(uuids))


def test_uuids_str():
    uuids = [str(uuid.uuid4()) for _ in range(10)]
    assert [uuid.UUID(id) for id in uuids] == list(filter_uuids(uuids))


def test_invalid_uuids_are_filtered_out():
    uuids = [str(uuid.uuid4()) for _ in range(10)]
    invalid_uuids = ["".join(random.choices(string.ascii_lowercase, k=32)), "42", ""]
    uuids.extend(invalid_uuids)
    filtered = list(filter_uuids(uuids))
    assert len(filtered) == 10
    for invalid_uuid in invalid_uuids:
        assert invalid_uuid not in filtered
