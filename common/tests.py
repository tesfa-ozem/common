import datetime


class ApproxDateTime:
    def __init__(
        self,
        value: datetime.datetime,
        abs: datetime.timedelta,
    ):
        self.value = value
        self.abs = abs

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, (datetime.datetime, datetime.date)):
            return NotImplemented

        if not isinstance(other, type(self.value)):
            raise ValueError

        diff = self.value - other
        return abs(diff) < self.abs
