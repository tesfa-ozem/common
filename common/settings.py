import functools
from typing import Callable, Type, TypeVar

from pydantic import BaseSettings

TSettings = TypeVar("TSettings", bound=BaseSettings)


def get_settings(cls: Type[TSettings]) -> TSettings:
    return cls()


get_settings = functools.lru_cache(get_settings)  # type: ignore


def settings_getter(settings_cls: Type[TSettings]) -> Callable[..., TSettings]:
    def inner() -> TSettings:
        return get_settings(settings_cls)

    return inner
