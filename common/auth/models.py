from pydantic import BaseModel


class AuthTokenPayload(BaseModel):
    sub: str
    iat: int
    exp: int

    username: str

    scopes: list[str] = []
    permissions: list[str] = []
