import jwt

from common.auth.models import AuthTokenPayload


class AuthenticationError(Exception):
    pass


def authenticate_user(
    token: str,
    public_key: str,
    algorithm: str = "RS256",
) -> AuthTokenPayload:
    if not token.startswith("Bearer "):
        raise AuthenticationError
    token = token.replace("Bearer ", "", 1)
    try:
        claims = jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=[algorithm],
        )
    except jwt.PyJWTError as e:
        raise AuthenticationError from e

    return AuthTokenPayload.parse_obj(claims)
